<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x410;&#x43c;&#x435;&#x440;&#x438;&#x43a;&#x430;&#x43d;&#x441;&#x43a;&#x438;&#x435; &#x433;&#x43e;&#x440;&#x43a;&#x438; &#x438; &#x43d;&#x430;&#x448;&#x438; &#x441;&#x442;&#x440;&#x430;&#x445;&#x438;" FOLDED="false" ID="ID_731775140" CREATED="1542481518915" MODIFIED="1542482064819" STYLE="oval">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_731775140" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="0.909" layout="OUTLINE">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<node TEXT="&#x43a;&#x440;&#x438;&#x442;&#x435;&#x440;&#x438;&#x438; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x44f; &#x441;&#x43b;&#x43e;&#x436;&#x43d;&#x43e;&#x441;&#x442;&#x438;" POSITION="right" ID="ID_1978964652" CREATED="1542481676758" MODIFIED="1542481774962">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="&#x443;&#x433;&#x43e;&#x43b; &#x43d;&#x430;&#x43a;&#x43b;&#x43e;&#x43d;&#x430;" POSITION="right" ID="ID_1318874998" CREATED="1542481838391" MODIFIED="1542481864385">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="&#x441;&#x43a;&#x43e;&#x440;&#x43e;&#x441;&#x442;&#x44c;" POSITION="right" ID="ID_1053502774" CREATED="1542481865700" MODIFIED="1542481875346">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="&#x43c;&#x43e;&#x434;&#x443;&#x43b;&#x44c; &#x443;&#x441;&#x43a;&#x43e;&#x440;&#x435;&#x43d;&#x438;&#x44f;" POSITION="right" ID="ID_572757751" CREATED="1542481882782" MODIFIED="1542481887869">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="&#x43f;&#x440;&#x43e;&#x434;&#x43e;&#x43b;&#x436;&#x438;&#x442;&#x435;&#x43b;&#x44c;&#x43d;&#x43e;&#x441;&#x442;&#x44c; &#x43f;&#x43e;&#x435;&#x437;&#x434;&#x43a;&#x438;" POSITION="right" ID="ID_1707225287" CREATED="1542481888249" MODIFIED="1542481929250">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="&#x432;&#x44b;&#x441;&#x43e;&#x442;&#x430; &#x43f;&#x43e;&#x434;&#x44a;&#x435;&#x43c;&#x430;" POSITION="right" ID="ID_382236433" CREATED="1542481802101" MODIFIED="1542481832541">
<edge COLOR="#0000ff"/>
</node>
</node>
</map>
